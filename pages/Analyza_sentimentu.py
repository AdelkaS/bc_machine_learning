import streamlit as st
import tweepy
import pytz
import datetime
import pickle
import os
import pandas as pd
import matplotlib.pyplot as plt
import altair as alt
from dotenv import load_dotenv
from wordcloud import WordCloud
import re
import preprocessor as p
import nltk
nltk.download('wordnet')
nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.tokenize import TweetTokenizer
from nltk.stem import SnowballStemmer
from deep_translator import GoogleTranslator
import time

@st.cache_resource
def loadModel(path):
    path = os.path.join(os.path.dirname(__file__), path)
    with open(path, 'rb') as f:
        vectorizer, model = pickle.load(f)
    return vectorizer, model

#@st.cache_data(show_spinner=False)
def loadTweet(_api, name):
    try:
        tweets = []
        for page in tweepy.Cursor(api.user_timeline, screen_name=name, include_rts = False, exclude_replies=True, tweet_mode = 'extended', count=200).pages(): #add number to pages to iterate first x pages
            tweets.extend(page)
        return tweets
    except tweepy.errors.TooManyRequests:
        st.warning('Někde nastala chyba. Zkuste to znovu za pár minut.', icon="⚠️")
        st.stop()
    except tweepy.errors.Unauthorized:
        st.warning('Od zadaného uživatele nelze získat data. Prosím zkontrolujte, zda účet existuje a je veřejný.', icon="⚠️")
        st.stop()
    except tweepy.errors.TweepyException:
        st.warning('Někde nastala chyba. Zkontrolujte zadané uživatelské jméno a zkuste to znovu.', icon="⚠️")
        st.stop()
    
def preprocesTweet(tweet):
    # nastaveni pro zpracovani textu
    p.set_options(p.OPT.URL, p.OPT.MENTION, p.OPT.HASHTAG, p.OPT.RESERVED)
    punctuation = '!"*$#%&\'()+,-./:;<=>?@[\\]^_`{|}~“’—–…”'   
    stemmer = SnowballStemmer("english")
    w_tokenizer = TweetTokenizer()
    stop_words = set(stopwords.words('english'))
    # ziskani textu, ktery chceme zpracovat
    process_text = tweet
     
    # odstraneni nepotrebnych veci (URL, hashtag..)
    process_text = p.clean(process_text)

    # prevedeni textu na mala pismena 
    process_text = process_text.lower()

    # odstraneni interpunkce
    for znak in punctuation:
        process_text = process_text.replace(znak, '')
    process_text = re.sub(r'\s+', ' ', process_text).strip()
    
    # odstraneni cisel
    process_text = re.sub(r'\d+', '', process_text)

    # steaming 
    process_text = [stemmer.stem(w) for w in \
                    w_tokenizer.tokenize((process_text))]

    # odstraneni "stop word"
    process_text = [w for w in process_text if not w in stop_words]

    process_text = ' '.join(process_text)

    return process_text

def preprocesTweet2(tweet):
    # nastaveni pro zpracovani textu
    p.set_options(p.OPT.URL, p.OPT.MENTION, p.OPT.HASHTAG, p.OPT.RESERVED)
    punctuation = '!"*$#%&\'()+,-./:;<=>?@[\\]^_`{|}~“’—–…”'
    # ziskani textu, ktery chceme zpracovat
    process_text = tweet
     
    # odstraneni nepotrebnych veci (URL, hashtag..)
    process_text = p.clean(process_text)

    # prevedeni textu na mala pismena 
    process_text = process_text.lower()

    # odstraneni interpunkce
    for znak in punctuation:
        process_text = process_text.replace(znak, '')
    process_text = re.sub(r'\s+', ' ', process_text).strip()
    
    # odstraneni cisel
    process_text = re.sub(r'\d+', '', process_text)

    return process_text

def writeLimits(_api):
    limit = api.rate_limit_status()
    #st.write(limit)
    st.write(limit["resources"]["statuses"]["/statuses/user_timeline"])
    time = datetime.datetime.fromtimestamp(limit["resources"]["statuses"]["/statuses/user_timeline"]["reset"], pytz.utc)
    local_time = time.astimezone(pytz.timezone('Europe/Prague'))
    st.write(local_time)

def sentiment_text(value):
    if value >= 0.6:
        return 'Pozitivní'
    elif 0.1 < value < 0.6:
        return 'Spíše pozitivní'
    elif -0.6 < value < -0.1:
        return 'Spíše negativní'
    elif value <= -0.6:
        return 'Negativní'
    else:
        return 'Neutrální'

def translate_month(month):
    translation_dict = {
        'January': 'Leden',
        'February': 'Únor',
        'March': 'Březen',
        'April': 'Duben',
        'May': 'Květen',
        'June': 'Červen',
        'July': 'Červenec',
        'August': 'Srpen',
        'September': 'Září',
        'October': 'Říjen',
        'November': 'Listopad',
        'December': 'Prosinec'
    }
    return translation_dict.get(month)

def translate_day(day):
    translation_dict = {
        'Monday': 'Pondělí',
        'Tuesday': 'Úterý',
        'Wednesday': 'Středa',
        'Thursday': 'Čtvrtek',
        'Friday': 'Pátek',
        'Saturday': 'Sobota',
        'Sunday': 'Neděle'
    }
    return translation_dict.get(day)

def process_tweets(tweets):
    dates = []
    sentiments = []
    texts = []

    for tweet in tweets:
        if(tweet.lang == 'en'):
            tweet_text = tweet.full_text
            text = preprocesTweet(tweet_text)
            sentiment = model.predict(vectorizer.transform([text]))
            dates.append(tweet.created_at)
            sentiments.append(sentiment[0])
            texts.append(preprocesTweet2(tweet_text))
        elif(tweet.lang == 'cs'):
            tweet_text = tweet.full_text
            translated_text = GoogleTranslator(source='czech', target='english').translate(tweet_text)
            text = preprocesTweet(translated_text)            
            sentiment = model.predict(vectorizer.transform([text]))
            dates.append(tweet.created_at)
            sentiments.append(sentiment[0])
            texts.append(preprocesTweet2(tweet_text))

    if(len(dates)==0):
        st.warning('Nebyl nalezen žádný tweet pro analýzu, prosím zkuste to s jiným uživatelem.', icon="⚠️")
        st.stop()
    
    df = pd.DataFrame({'date': dates, 'sentiment': sentiments, 'text': texts}) 
    df['date'] = pd.to_datetime(df['date'], utc=True)
    df['sentiment_text'] = df['sentiment'].map({-1:'Negativní', 1:'Pozitivní'})
    
    return df

def create_graph(df):
    df['mean_5'] = df['sentiment'].rolling(5, center=True, min_periods=3).mean()
    df['mean_20'] = df['sentiment'].rolling(20, center=True, min_periods=12).mean()
    df['mean_100'] = df['sentiment'].rolling(100, center=True, min_periods=56).mean()

    selection = alt.selection_multi(fields=['key'], bind='legend')
    point = alt.Chart(df).mark_circle(size=60, color='#29b09d').encode(
        alt.X('date:T', 
            title='Datum',
            axis=alt.Axis(format="%d.%m %Y"),
            ),  
        alt.Y('sentiment', 
            title='Sentiment',
            scale=alt.Scale(domain=[-1, 1]),
            axis=alt.Axis(labelExpr="datum.label == 1 ? 'Pozitivní' : 'Negativní'",
                            values=[-1, 1],
                            labelAngle=-90
                            )
            ),
        tooltip=[alt.Tooltip('date', format='%d.%m %Y %H:%M', title='Datum'), 
                alt.Tooltip('sentiment_text', title='Sentiment')]        
    )

    center_line = alt.Chart(pd.DataFrame({'y': [0], 'description':'Neutrální'})).mark_rule(color='grey').encode(
        y='y',
        tooltip=alt.Tooltip('description')
    )        
        
    lines = alt.Chart(df).transform_fold(
        ['mean_5', 'mean_20', 'mean_100'],
    ).mark_line(
        interpolate='monotone'
    ).encode(
        x='date:T',
        y=alt.Y('value:Q'),
        color=alt.Color('key:N', 
                        legend=alt.Legend(
                            labelExpr="datum.label == 'mean_5' ? 'Krátkodobý průměrný sentiment tweetů' : datum.label == 'mean_20' ? 'Střednědobý průměrný sentiment tweetů' : datum.label == 'mean_100' ? 'Dlouhodobý průměrný sentiment tweetů' : ''",
                            title=None,
                            orient='bottom',
                            direction='vertical',
                            labelLimit=0
                        )
                        ),
        opacity=alt.condition(selection, alt.value(1), alt.value(0.2)),
        tooltip=[alt.Tooltip('date', format='%d.%m %Y', title='Datum')]
    ).add_selection(
        selection
    )

    chart = (point + center_line + lines).properties(height=600, title="Graf sentimentu tweetů")
    st.altair_chart(chart, use_container_width=True)

def create_month_graph(df):
    df_month = df.groupby([df['date'].dt.year.rename('year'), df['date'].dt.month.rename('month')]).aggregate(
        date = ('date', 'first'),
        tweet_count = ('sentiment', 'count'),
        mean_sentiment = ('sentiment', 'mean'),
        positive_tweet = ('sentiment', lambda x: (x == 1).sum()),
        negative_tweet = ('sentiment', lambda x: (x == -1).sum())
    ).reset_index()

    df_month = df_month.drop(['year', 'month'], axis=1)
    df_month['date'] = pd.to_datetime(df_month['date']).dt.strftime('%m/%y')

    df_month['mean_sentiment_3'] = df_month['mean_sentiment'].rolling(3, center=True, min_periods=1).mean()
    df_month['mean_sentiment_6'] = df_month['mean_sentiment'].rolling(6, center=True, min_periods=1).mean()
    max = df_month['tweet_count'].max()

    selection = alt.selection_multi(fields=['key'], bind='legend')

    counts = alt.Chart(df_month).transform_fold(
        ['negative_tweet', 'positive_tweet', 'tweet_count']
    ).mark_line(
        interpolate='monotone'
    ).encode(
        x=alt.X('date:O', 
                sort=None, 
                title='Datum',
                axis=alt.Axis(
                    labelAngle=-45
                )
                ),
        y=alt.Y('value:Q', 
                scale=alt.Scale(domain=[0, max + 10]),
                axis=alt.Axis(tickCount=6),  
                title='Počty tweetů',                  
                ),
        color=alt.Color('key:N', 
                        legend=alt.Legend(
                            labelExpr="datum.label == 'negative_tweet' ? 'Počet negativních tweetů' : datum.label == 'positive_tweet' ? 'Počet pozitivních tweetů' : datum.label == 'tweet_count' ? 'Celkový počet tweetů' : ''",
                            title=None,
                            orient='bottom',
                            direction='vertical',
                            labelLimit=0
                        )
                        ),
        opacity=alt.condition(selection, alt.value(1), alt.value(0.2)),
        tooltip=[alt.Tooltip('date', title='Datum'),
                alt.Tooltip('value:Q', title='Počet tweetů')
                ]
    ).add_selection(
        selection
    ).properties(
        height=600,
        title="Graf počtu tweetů v průběhu měsíců",                
    )

    st.altair_chart(counts, use_container_width=True)

    positivity = alt.Chart(df_month).transform_fold(
        ['mean_sentiment', 'mean_sentiment_3', 'mean_sentiment_6']
    ).mark_line(
        interpolate='monotone'
    ).encode(
        x=alt.X('date:O', 
                sort=None, 
                title='Datum',
                axis=alt.Axis(
                    labelAngle=-45
                )
                ),
        y=alt.Y('value:Q', 
            title='Sentiment',
            scale=alt.Scale(domain=[-1, 1]),
            axis=alt.Axis(labelExpr="datum.label == 1 ? 'Pozitivní' : 'Negativní'",
                            values=[-1, 1])
            ),
        color=alt.Color('key:N', 
                    legend=alt.Legend(
                        labelExpr="datum.label == 'mean_sentiment' ? 'Průměrný sentiment měsíce' : datum.label == 'mean_sentiment_3' ? 'Průměrný sentiment 3 měsíců' : datum.label == 'mean_sentiment_6' ? 'Průměrný sentiment 6 měsíců' : ''",
                        title=None,
                        orient='bottom',
                        direction='vertical',
                        labelLimit=0
                    )
                    ),
        tooltip=[alt.Tooltip('date', title='Datum'),
                ],
        opacity=alt.condition(selection, alt.value(1), alt.value(0.2))
    ).add_selection(
        selection
    )
    
    center_line = alt.Chart(pd.DataFrame({'y': [0], 'description':'Neutrální'})).mark_rule(color='grey').encode(
        y='y',
        tooltip=alt.Tooltip('description')
    )

    chart = (positivity + center_line).properties(height=600, title="Graf sentimentu tweetů v průběhu měsíců")
    st.altair_chart(chart, use_container_width=True)

def create_months_graph(df):
    df_months = df.groupby(df['date'].dt.month_name().rename('month')).aggregate(
        tweet_count=('sentiment', 'count'),
        mean_sentiment=('sentiment', 'mean'),
        positive_tweet=('sentiment', lambda x: (x == 1).sum()),
        negative_tweet=('sentiment', lambda x: (x == -1).sum())
    ).reset_index()

    month_order = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    df_months['month'] = pd.Categorical(df_months['month'], categories=month_order, ordered=True)
    df_months = df_months.sort_values('month')
    df_months['month'] = [translate_month(month) for month in df_months['month']]
    df_months['sentiment_text'] = df_months['mean_sentiment'].apply(sentiment_text)

    bars = alt.Chart(df_months).mark_bar().encode(
        x = alt.X('month',
                    sort=None,
                    axis=alt.Axis(labelAngle=0),
                    title='Měsíc'
                    ),
        y = alt.Y('mean_sentiment', 
            title='Sentiment',
            scale=alt.Scale(domain=[-1, 1]),
            axis=alt.Axis(labelExpr="datum.label == 1 ? 'Pozitivní' : 'Negativní'",
                            values=[-1, 1],
                            labelAngle=-90
                            )
            ),
        color=alt.Color(
            'mean_sentiment',
            scale=alt.Scale(
                domain=[-1, -0.1, 0, 0.1, 1],
                range=['#e60000', '#ff9999', '#ffffff', '#4dffd2', '#00664d'],
            ),
            legend=None,
        ),
        tooltip = [alt.Tooltip('month', title='Měsíc'),
                    alt.Tooltip('sentiment_text', title='Sentiment')
                    ]
    )

    center_line = alt.Chart(pd.DataFrame({'y': [0], 'description':'Neutrální'})).mark_rule(color='grey').encode(
        y='y',
        tooltip=alt.Tooltip('description')
    )  

    chart = (bars + center_line).properties(height=400, title="Sentiment v jednotlivých měsících")
    st.altair_chart(chart, use_container_width=True)

def create_days_graph(df):
    df_days = df.groupby(df['date'].dt.day_name().rename('day')).aggregate(
        tweet_count=('sentiment', 'count'),
        mean_sentiment=('sentiment', 'mean'),
        positive_tweet=('sentiment', lambda x: (x == 1).sum()),
        negative_tweet=('sentiment', lambda x: (x == -1).sum())
    ).reset_index()

    day_order = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
    df_days['day'] = pd.Categorical(df_days['day'], categories=day_order, ordered=True)
    df_days = df_days.sort_values('day')
    df_days['day'] = [translate_day(day) for day in df_days['day']]
    df_days['sentiment_text'] = df_days['mean_sentiment'].apply(sentiment_text)

    bars = alt.Chart(df_days).mark_bar().encode(
        x = alt.X('day',
                    sort=None,
                    axis=alt.Axis(labelAngle=0),
                    title='Den'
                    ),
        y = alt.Y('mean_sentiment', 
            title='Sentiment',
            scale=alt.Scale(domain=[-1, 1]),
            axis=alt.Axis(labelExpr="datum.label == 1 ? 'Pozitivní' : 'Negativní'",
                            values=[-1, 1],
                            labelAngle=-90)
            ),
        color=alt.Color(
            'mean_sentiment',
            scale=alt.Scale(
                domain=[-1, -0.1, 0, 0.1, 1],
                range=['#e60000', '#ff9999', '#ffffff', '#4dffd2', '#00664d'],
            ),
            legend=None,
        ),
        tooltip = [alt.Tooltip('day', title='Den'),
                    alt.Tooltip('sentiment_text', title='Sentiment')
                    ]
    )

    center_line = alt.Chart(pd.DataFrame({'y': [0], 'description':'Neutrální'})).mark_rule(color='grey').encode(
        y='y',
        tooltip=alt.Tooltip('description')
    )  

    chart = (bars + center_line).properties(height=400, title="Sentiment v jednotlivých dnech")
    st.altair_chart(chart, use_container_width=True)

def create_word_cloud(df):
    text_for_cloud = ' '.join(df['text'])
    word_cloud = WordCloud(width=740, 
                            height=500, 
                            background_color='#0e1117',
                            colormap='Set2_r',
                            collocations = False).generate(text_for_cloud)
    wordcloud_image = word_cloud.to_image()
    st.image(wordcloud_image, use_column_width=True)

def write_statistic(df):
    count = df.shape[0]
    date_to = df['date'][0].strftime("%d.%m. %Y %H:%M")
    date_from = df['date'][count-1].strftime("%d.%m. %Y %H:%M")
    days = (df['date'][0] - df['date'][count-1]).days + 2
    years = round(days/365, 2)
    positive = len(df[df['sentiment']==1])
    positive_perc = int(positive/count*100)
    negative = count - positive
    negative_perc = 100 - positive_perc
    tweet_ratio = round(count/days, 2)
    mean_sentiment = sentiment_text(df['sentiment'].mean()).lower()
    
    st.write('Byly analyzovány tweety od ', date_from, ' do ', date_to, ', což je celkem ', str(days), ' dnů (' + str(years), 'roku)')
    st.write('Celkem bylo analyzováno', str(count), 'tweetů, z čeho je ', str(positive), '(' + str(positive_perc) + '%) tweetů pozitivních')
    st.write('Celkově jsou tweety ' + mean_sentiment)
    st.write('Tento uživatel tweetuje průměrně', str(tweet_ratio) + 'x za den')

load_dotenv()

ak = os.getenv("API_KEY")
aks = os.getenv("API_SECRET") 
at = os.getenv("ACCESS_TOKEN")
ats = os.getenv("ACCESS_SECRET")


auth = tweepy.OAuthHandler(ak, aks)
auth.set_access_token(at, ats)

api = tweepy.API(auth)

try:
    api.verify_credentials()
except:
    st.warning('Připojení k twiteru selhalo, prosím zkusto to znovu.', icon="⚠️")
    st.stop()

vectorizer, model = loadModel('models/sentiment_analysis.sav')

#writeLimits(api)

st.title("Analýza sentimentu tweetů")
st.write('Do kolonky níže vložte nějaké uživatelské jméno uživatele twittteru a po kliknutí na start se provede analýza sentimentu a vytvoří se několik zajímavých grafů. Analýza sentimentu slouží k rozhodování, zda je tweet pozitivní nebo negativní.')
st.info('Můžete zadat druhé uživatelsk jméno pro porovnávání. Ale není to nutné!', icon="ℹ️")
st.warning('Můžete zadat i české účty, ale zpracování bude trvat déle.', icon="⚠️")

st.text_input(
        "Zadejte uživatelské jméno",
        "BarackObama",
        key="name",
    )

st.text_input(
        "Zadejte druhé uživatelské jméno pro porovnání",
        key="name2",
    )
   
if st.button("Analyzuj tweety"):
    if st.session_state.name2:
        # analyza pro 2 lidi
        with st.spinner('Probíhá stahování tweetů'):
            tweets = loadTweet(api, st.session_state.name)
            tweets2 = loadTweet(api, st.session_state.name2)
        start_time = time.time()
        with st.spinner('Probíhá analýza tweetů'):
            df = process_tweets(tweets)
            df2 = process_tweets(tweets2)

        selection = alt.selection_multi(fields=['key'], bind='legend')

        graph, month_graph, days, months, words, statistics = st.tabs(["Souhrný graf", "Analýza po měsících", "Analýza dnů", "Analýza měsíců", "Nejpoužívanější slova", "Statistika"])
        
        with graph:
            col1, col2 = st.columns(2)
            with col1:
                st.subheader(st.session_state.name)
                create_graph(df)
            with col2:
                st.subheader(st.session_state.name2)
                create_graph(df2)

        with month_graph:
            col1, col2 = st.columns(2)
            with col1:
                st.subheader(st.session_state.name)
                create_month_graph(df)
            with col2:
                st.subheader(st.session_state.name2)
                create_month_graph(df2)
            
        with days:
            col1, col2 = st.columns(2)
            with col1:
                st.subheader(st.session_state.name)
                create_days_graph(df)
            with col2:
                st.subheader(st.session_state.name2)
                create_days_graph(df2)
        
        with months:
            col1, col2 = st.columns(2)
            with col1:
                st.subheader(st.session_state.name)
                create_months_graph(df)
            with col2:
                st.subheader(st.session_state.name2)
                create_months_graph(df2)

        with words:
            col1, col2 = st.columns(2)
            with col1:
                st.subheader(st.session_state.name)
                create_word_cloud(df)
            with col2:
                st.subheader(st.session_state.name2)
                create_word_cloud(df2)

        with statistics:
            col1, col2 = st.columns(2)
            with col1:
                st.subheader(st.session_state.name)
                write_statistic(df)
            with col2:
                st.subheader(st.session_state.name2)
                write_statistic(df2)  
            st.divider()   
            end_time = time.time()
            st.write('Celá analýza trvala', str(round(end_time - start_time, 2)), 'sekund')
    else: 
        with st.spinner('Probíhá stahování tweetů'):
            tweets = loadTweet(api, st.session_state.name)
        start_time = time.time()
        with st.spinner('Probíhá analýza tweetů'):
            df = process_tweets(tweets)

        selection = alt.selection_multi(fields=['key'], bind='legend')

        graph, month_graph, days, months, words, statistics = st.tabs(["Souhrný graf", "Analýza po měsících", "Analýza dnů", "Analýza měsíců", "Nejpoužívanější slova", "Statistika"])
        
        with graph:
            create_graph(df)

        with month_graph:
            create_month_graph(df)
            
        with days:
            create_days_graph(df)
        
        with months:
            create_months_graph(df)

        with words:
            create_word_cloud(df)

        with statistics:
            write_statistic(df)   
            st.divider()      
            end_time = time.time()
            st.write('Celá analýza trvala', str(round(end_time - start_time, 2)), 'sekund')