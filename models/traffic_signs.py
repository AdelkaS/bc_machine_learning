import tensorflow as tf
from tensorflow import keras
import pickle
import numpy as np
from keras import layers
from keras import models
import matplotlib.pyplot as plt
from keras.callbacks import ModelCheckpoint
import os

# funkce pro nacteni dat
def loadData(path):
    path = os.path.join(os.path.dirname(__file__), path)
    file = open(path, "rb")
    data = pickle.load(file)
    file.close()
    return data

# nacteni dat
train = loadData("data/train.p")
valid = loadData("data/valid.p")
test = loadData("data/test.p")

# ulozeni priznaku a kategorii
train_data = train["features"]
train_label = train["labels"]

valid_data = valid["features"]
valid_label = valid["labels"]

test_data = test["features"]
test_label = test["labels"]

# promíchání dat
tf.random.shuffle(train_data, seed=5)
tf.random.shuffle(train_label, seed=5)

# vrstvy pro preprocessing obrazku
resize_and_rescale = models.Sequential([
  layers.Resizing(32, 32, input_shape=(None, None, 3)),
  layers.Rescaling(1./255)
])

# vrstvy pro augmentaci dat
data_augmentation = models.Sequential([
  layers.RandomZoom(.2,.2),
  layers.RandomRotation(.15),
  layers.RandomTranslation(.2, .2),
])

# tvorba modelu
model = models.Sequential([
  # pridani preprocesnich vrstev
  resize_and_rescale,
  data_augmentation,

  # konvolucni vrstvy
  layers.Conv2D(256, (3, 3), activation='relu'),
  layers.MaxPooling2D((2, 2)),
  layers.Conv2D(512, (3, 3), activation='relu'),
  layers.MaxPooling2D((2, 2)),
  layers.Conv2D(1024, (3, 3), activation='relu'),
  layers.MaxPooling2D((2, 2)),

  # densy vrstvy pro predikci
  layers.Flatten(),
  layers.Dropout(0.5),
  layers.Dense(1024, activation='relu'),
  layers.Dense(512, activation='relu'),
  layers.Dense(43, activation='softmax'),
])

# nastaveni modelu
model.compile(optimizer=tf.keras.optimizers.Adam(epsilon=0.01),
  loss='SparseCategoricalCrossentropy',
  metrics=['accuracy'])

# nastaveni checkpointu pro ulozeni nejlepsiho modelu
model_checkpoint_callback = ModelCheckpoint(
    filepath='tmp_best.h5',
    save_weights_only=False,
    monitor='val_accuracy',
    mode='max',
    save_best_only=True)

# trenovani modelu
model_info = model.fit(train_data, 
                       train_label, 
                       batch_size=256, #16
                       epochs=1, #100
                       callbacks=[model_checkpoint_callback],
                       validation_data=(valid_data, valid_label))

# ulozeni modelu
model.save('traffic_signs.h5')

# vyhodnoceni modelu na zaklade tranovacich a validovacich dat
acc = model_info.history['accuracy']
val_acc = model_info.history['val_accuracy']
loss = model_info.history['loss']
val_loss = model_info.history['val_loss']
epochs = range(1, len(acc) + 1)


# vykresleni grafu
plt.plot(epochs[10:], acc[10:], 'r', label='Trénovací přesnost')
plt.plot(epochs[10:], val_acc[10:], 'b', label='Validační přesnost')
plt.title('Trénovační a validační přesnost od epochy 11')
plt.legend()
plt.figure()
plt.plot(epochs[10:], loss[10:], 'r', label='Trénovací ztráta')
plt.plot(epochs[10:], val_loss[10:], 'b', label='Validační ztráta')
plt.title('Trénovační a validační  ztráta od epochy 11')
plt.legend()
plt.show()

# metriky
accuracy = model.evaluate(test_data, test_label)
print("Úspěšnost modelu na testovacích datech: " + str(accuracy))