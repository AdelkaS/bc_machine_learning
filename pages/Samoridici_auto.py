import streamlit as st
import streamlit.components.v1 as components

st.title('Samořídící auto')
st.write('V aplikaci níže se auto naučí samo jezdit a nenarážet do zní. Stačí vybrat dráhu a stisknout start. Můžete si zkoušet měnit různé parametry, ale pro správnou funkčnost není potřeba nic měnit.')
components.iframe("https://adelasvitilova.github.io/self-driving-car-web/", width=740, height=500)