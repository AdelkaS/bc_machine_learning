# Ukázky využití strojového učení

## Spuštění
Pro spuštění stačí do terminálu napsat: `streamlit run Uvodni_stranka.py`

## Zprovoznění analýzy sentimentu
Pro fungování analýzy sentimentu je potřeba si do složky pages vytvořit soubor .env s přístupovými údaji k vývojářskému twitter účtu.
Struktura .env souboru:

API_KEY = 'XXXXXX'

API_SECRET = 'XXXXXX'

ACCESS_TOKEN = 'XXXXXX'

ACCESS_SECRET = 'XXXXXX'

## Využité knihovny 

- Altair (Licence: BSD 3-Clause; Copyright (c) 2015-2023, Vega-Altair Developers All rights reserved.)
- Deep-translator (Licence: Apache 2.0; Copyright (c) June 2020 Nidhal Baccouri)
- NLTK (Licence: Apache 2.0)
- OpencCV-python (Licence: MIT; Copyright (c) Olli-Pekka Heinisuo)
- Python-DotEnv (Licence: BSD 3-Clause; Copyright (c) 2014, Saurabh Kumar (python-dotenv), 2013, Ted Tieken (django-dotenv-rw), 2013, Jacob Kaplan-Moss (django-dotenv))
- Scikit-learn (Licence: BSD 3-Clause; Copyright (c) 2007-2023 The scikit-learn developers. All rights reserved.)
- Streamlit (Licence: Apache 2.0)
- TensorFlow (Licence: Apache 2.0; All contributions by the University of California: Copyright (c) 2014, The Regents of the University of California (Regents) All rights reserved.; All other contributions: Copyright (c) 2014, the respective contributors All rights reserved.)
- Tweepy (Licence: MIT; Copyright (c) 2009-2023 Joshua Roesslein)
- Tweet-preprocessor (Licence: GNU General Public v3.0; Copyright (C) 2007 Free Software Foundation, Inc)
- WordCloud (Licence: MIT; Copyright (c) 2012 Andreas Christian Mueller)