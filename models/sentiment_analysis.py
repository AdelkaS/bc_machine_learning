import pandas as pd
import numpy as np
import re
import preprocessor as p
import matplotlib.pyplot as plt
import nltk
nltk.download('wordnet')
nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.tokenize import TweetTokenizer
from nltk.stem import SnowballStemmer
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
import pickle
import os

# nastaveni pro zpracovani textu
p.set_options(p.OPT.URL, p.OPT.MENTION, p.OPT.HASHTAG, p.OPT.RESERVED) # p.OPT.EMOJI, p.OPT.SMILEY, p.OPT.NUMBER
punctuation = '"#&\'()+,-./:;<=>@[\\]^_`{|}~“’%$!?*'
stemmer = SnowballStemmer("english")
w_tokenizer = TweetTokenizer()
stop_words = set(stopwords.words('english'))

def preprocess_tweet(row):
    # ziskani textu, ktery chceme zpracovat
    process_text = row['text']
     
    # odstraneni nepotrebnych veci (URL, hashtag..)
    process_text = p.clean(process_text)

    # prevedeni textu na mala pismena 
    process_text = process_text.lower()

    # odstraneni interpunkce
    for znak in punctuation:
        process_text = process_text.replace(znak, '')
    process_text = re.sub(r'\s+', ' ', process_text).strip()
    
    # odstraneni cisel
    process_text = re.sub(r'\d+', '', process_text)

    # steaming 
    process_text = [stemmer.stem(w) for w in \
                    w_tokenizer.tokenize((process_text))]
    
    # odstraneni "stop word"
    process_text = [w for w in process_text if not w in stop_words]

    process_text = ' '.join(process_text)

    return process_text

# nacteni dat
path = os.path.join(os.path.dirname(__file__), "data/Tweets.csv")
data = pd.read_csv(path).dropna()
df1 = pd.DataFrame({'sentiment': data['sentiment'], 'text': data['text']})
df1['sentiment'] = df1['sentiment'].replace({"negative":-1, "neutral":0, "positive":1})

path = os.path.join(os.path.dirname(__file__), "data/Tweets2.csv")
data = pd.read_csv(path)
df2 = pd.DataFrame({'sentiment': data['sentiment'], 'text': data['text']})
df2 = df2.dropna()
df2['sentiment'] = df2['sentiment'].replace({"negative":-1, "neutral":0, "positive":1})

df = pd.concat([df1, df2])

# odstraneni neutralnich hodnot
df = df[df.sentiment != 0]

# vytvoreni trenovaci a testovaci mnoziny
df_train = df.sample(frac=0.8, random_state=42)
df_test = df.drop(df_train.index)

# predzpracovani trenovaci mnoziny
df_train['text'] = df_train.apply(preprocess_tweet, axis=1)
X_train = df_train['text']
y_train = df_train['sentiment']

# predzpracovani testovaci mnoziny
df_test['text'] = df_test.apply(preprocess_tweet, axis=1)
X_test = df_test['text']
y_test = df_test['sentiment']

# vektorizace
vectorizer = CountVectorizer(ngram_range=(1,2)).fit(X_train)
X_train_vectorized = vectorizer.transform(X_train)

# vytvoreni modelu
model = MultinomialNB()
model.fit(X_train_vectorized, y_train)

# vysledky modelu
predictions = model.predict(vectorizer.transform(X_train))
print("Přesnost na trénovací množině:", 100 * sum(predictions == y_train) / len(predictions), '%')
predictions = model.predict(vectorizer.transform(X_test))
print("Přesnost na testovací množině:", 100 * sum(predictions == y_test) / len(predictions), '%')

# vytvoreni a vypsani matice zamen
cm = confusion_matrix(y_test, predictions)
disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=["Negativní", "Pozitivní"])
disp.plot()

plt.xlabel("Predikovaná hodnota")
plt.ylabel("Cílová hodnota")
plt.show()

# ulozeni modelu
filename = 'sentiment_analysis.sav'
with open(filename, 'wb') as f:
    pickle.dump((vectorizer, model), f)