import streamlit as st
import tensorflow as tf
import os
import pandas as pd
import numpy as np
import cv2
 
@st.cache_resource
def loadModel(path):
    path = os.path.join(os.path.dirname(__file__), path)
    model = tf.keras.models.load_model(path)
    return model

@st.cache_data
def loadCSV(path):
    path = os.path.join(os.path.dirname(__file__), path)
    file = pd.read_csv(path)
    return file

def predict(img_tensor):
    prediction = model.predict(np.array([img_tensor]))
    names['Probability'] = prediction.T * 100
    sort_prediction = names.sort_values(by=['Probability'], ascending=False)
    sort_prediction['Probability'] = sort_prediction['Probability'].round(0).astype(int)
    sort_prediction['Probability'] = sort_prediction['Probability'].astype(str) + '%'
    predict_class = np.argmax(prediction)
    res = names[names['ClassId'] == predict_class]['SignNameCzech'].values[0]
    
    # vypis vysledku
    with result.container():  
        st.header("Značka je nejpravděpodobněji: " + res)
        st.text("Tři nejpravděpodobnější značky jsou:")
        table_style = """
                    <style>
                    thead {display:none}
                    tbody th {display:none}                   
                    tbody td:nth-child(2) {width: 65% !important}
                    </style>
                    """    
        st.markdown(table_style, unsafe_allow_html=True)
        st.table(sort_prediction[:3][['SignNameCzech','Probability']])

# nacteni modelu a oznaceni
names = loadCSV('static/signnames.csv')
model = loadModel('models/traffic_signs.h5')

# vyber kamery
cam = cv2.VideoCapture(0)

st.title('Rozpoznávání dopravních značek')
st.write('Po kliknutí na start začne webkamera snímat, dejte před ní nějakou dopravní značku a program se bude snažit uhodnout co za značku právě vidí.')
st.info('Pokud program značku nerozeznává je možná, že tuto značku ani nezná, má jen omezený počet dopravních značek, co zná.', icon="ℹ️")
st.warning('Pro co nejlepší výsledek načtěte značku doprostřed okna co největší to jde.', icon="⚠️")

with st.expander("Rozpoznávané dopravní značky"):
    table_style = """
                    <style>
                    thead {display:none}
                    tbody th {display:none}                   
                    </style>
                    """    
    st.markdown(table_style, unsafe_allow_html=True)
    st.table(names['SignNameCzech'])

run = st.button("Start") 

camera_input = st.empty()
result = st.empty()

i = 0
while run:
    ret, frame = cam.read()
    if ret:
        i += 1
        # prevedeni na rgb
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        # zobrazeni nacteneho obrazku
        camera_input.image(frame)
        # predikce
        if(i==15):
            predict(frame)
            i=0
    else:
        result.text("Někde nastala chyba!")
